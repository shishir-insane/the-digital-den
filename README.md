# The Digital Den of Shishir Kumar

This projects contains snippets, and files used on the blog - [The Digital Den](https://thedigitalden.substack.com/?showWelcome=true)

## Posts

| Post | Project Directory |
|-------|------------------|
|[Secure Your Home Server Traffic with Let's Encrypt: A Step-by-Step Guide to Nginx Proxy Manager using Docker Compose](https://thedigitalden.substack.com/p/lets-encrypt-nginx-proxy-manager-docker-compose)|`lets-encrypt-nginx-proxy-manager-docker-compose`|
|[How to Install Vaultwarden with Docker Compose: A Step-by-Step Guide](https://thedigitalden.substack.com/p/install-vaultwarden-with-docker-compose-guide)|`install-vaultwarden-with-docker-compose-guide`|
|[Step-by-Step Guide: Installing Traefik with Cloudflare and Let's Encrypt for Secure and Scalable Web Applications](https://thedigitalden.substack.com/p/guide-traefik-with-cloudflare-lets-encrypt)|`guide-traefik-with-cloudflare-lets-encrypt`|
|[Self-Host a Ghost Blog on Your Home Server using Portainer and Docker Compose - Step-by-Step Guide](https://thedigitalden.substack.com/p/guide-self-host-ghost-docker-portainer-steps)|`guide-self-host-ghost-docker-portainer-steps`|
